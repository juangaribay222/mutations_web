INSTALACION PARA USO DE LOCALHOST DEL PROYECTO

Guardar el siguiente proyecto en un directorio
https://gitlab.com/juangaribay222/mutations_web.git
    -> Con comando GIT:
        git clone git@gitlab.com:juangaribay222/mutations_web.git
    -> RAR
        Descargar el proyecto en RAR

Abrir el directorio donde este el proyecto y abrir la consola
Ejecutar el siguiente comando
    -> Comando para instalar paquetes
        npm install

Una vez que se haya terminado la instalacion 
Ejecutar el siguiente comando
    -> Comando de para abrir proyecto en localhost
        ng serve

Ahora solo queda abrir el enlace
    -> Enlace
        http://localhost:4200/mutations