import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IMutation } from '../../Model/imutation';
import { MutationService } from '../../Service/mutation.service';
import { IStats } from '../../Model/istats';

@Component({
  selector: 'app-mutation',
  templateUrl: './mutation.component.html',
  styleUrls: ['./mutation.component.css']
})
export class MutationComponent implements OnInit {
  isReadOnly: boolean = true;
  mutationControl: FormGroup;
  mutationList:Array<IMutation>=[];
  mutationId:number=0;
  title:string="Mutation";
  isActive=true;
  isEditing=false;
  isDetail=false;
  isUpdating=false;
  isMutationTrue=false;
  viewStatsMutation=false;
  viewStatsAll=false;
  statsOfMutation:any;
  arrayOfStatsOfMutation:Array<IStats>=[];
  yesMutations=0;
  noMutations=0;
  ratio:any;
  constructor(private _mutationService: MutationService) {
    this.mutationControl = this.createFormGroup();
   }

  ngOnInit(): void {
    this.fillGridMutationsStats();
  }
  createFormGroup(){
    this.mutationId=0;
    return new FormGroup({
			id: new FormControl(this.mutationId.toString()),
			date: new FormControl(new Date().toLocaleDateString('en-US', { hour12: false }) ),
			secuence: new FormControl('', [Validators.required,Validators.maxLength(36),Validators.minLength(36)]),
      isMutation: new FormControl(`${false}`)
    })
  }
  onResetForm(){
    this.cleanStats();
    this.isMutationTrue = false;
    this.mutationControl.controls["isMutation"].setValue(false)
    this.mutationControl.reset();
    this.mutationControl = this.createFormGroup();
  }
  async saveMutation(){
    this.mutationControl.value.secuence = this.mutationControl.value.secuence.trim();
    this.mutationControl.value.date = new Date().toLocaleDateString();    
    let isCreated = await this._mutationService.addMutation(this.mutationControl.value);
    if (isCreated != null) {
      isCreated.subscribe(
        ()=>{
        this.fillGridMutationsStats();
        this.onResetForm();
        alert('El registro ha sido guardado éxitosamente '+'¡Guardado!')
        }
      )
    }
    else{
      console.error("ERRORR!! Ya existe esta cadena");
    }
  }
  fillGridMutationsStats(){
    this._mutationService.getMutations().subscribe(
      (data) => {

        this.mutationList = data
      },
      (error) => console.error(error)
      
    )
  }
  randomSecuence(){
    this.onResetForm();
    let secuence:string;
		secuence = this.generateRandomString(36);
		this.mutationControl.controls["secuence"].setValue(secuence.trim())
	}
	generateRandomString = (num:number) => {
		const characters ='ATCG';
		let res= ' ';
		const charactersLength = characters.length;
		for ( let i = 0; i < num; i++ ) {
			res += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		
		return res;
	}
  async seeHasMutation(){
    this._mutationService.hasMutation;
  }
  async verifyMutation(){
    let responseStats:IStats;
    let isMutation;
    let blocksMutation;
    let secuenceADN = this.mutationControl.value.secuence.trim();
    

    let isMutant = await this._mutationService.hasMutation(secuenceADN).subscribe(
      (data) => {        
         isMutation = data.isMutation;
         blocksMutation = data.blocks;
        responseStats = {
          isMutation:isMutation,
          blocks:blocksMutation,
          secuence:secuenceADN
        };
        this.statsOfMutation = responseStats;
        this.isMutationTrue = this.statsOfMutation.isMutation;
        this.mutationControl.controls["isMutation"].setValue(this.statsOfMutation.isMutation)
        alert(`La secuencia ${this.statsOfMutation.secuence} ${this.statsOfMutation.isMutation === true? 'Si': 'No'} tiene mutacion`)
      },
      err => {
        console.error(err.message);
      }
    )
  }
  async openStatsOfMutation(secuence:any){
    let blocksMutation:Array<string>[];
    let responseStats:IStats=<IStats>{};
    let isMutation;
    let secuenceMutation = secuence;
    let stats = await this._mutationService.getStatsOfMutation(secuenceMutation)
    .subscribe(
      res => {

        blocksMutation = res;
        isMutation = res.isMutation;
        responseStats = {
          isMutation:isMutation,
          blocks:blocksMutation,
          secuence:secuenceMutation
        };
        this.statsOfMutation = responseStats.blocks
        this.viewStatsMutation = true;
      }
    )
  }
  cleanStats(){
    this.arrayOfStatsOfMutation=[];
    this.statsOfMutation={};
    this.viewStatsMutation = false;
    this.yesMutations=0;
    this.viewStatsAll=false
    this.noMutations=0;
    this.ratio=0;
  }
  viewStats(){
    let statsMutationList = [];
    
    this.mutationList.forEach(
      (x,i) => {
      this._mutationService.getStatsOfMutation(x.secuence).subscribe(
        (res) => {
          this.viewStatsAll=true;
          statsMutationList.push();
          this.arrayOfStatsOfMutation.push(res)
          if(res.isMutation)
            this.yesMutations++;
          else
            this.noMutations++;
        })
        this.ratio = (this.yesMutations / this.noMutations);
      });
  }
  editSecuence(){
    this.isReadOnly = !this.isReadOnly;
  }
  get secuence() {return this.mutationControl.get('secuence')}
  get isMutation(){return this.mutationControl.get('isMutation')}
}
