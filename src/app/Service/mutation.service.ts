
import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { IMutation } from '../Model/imutation';

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

@Injectable({ providedIn: "root" })
export class MutationService {
  private path: string = "https://floating-retreat-07648.herokuapp.com/mutations/";
  constructor(private http: HttpClient) {}
  
  getMutations(): Observable<IMutation[]> {
    return this.http.get<IMutation[]>(this.path).pipe(retry(1));
  }
  getMutation(id: string): Observable<IMutation> {
    return this.http.get<IMutation>(this.path + id).pipe();
  }
  async addMutation(mutation: IMutation) {
    var isCreated;
    await this.getMutations().forEach(mutations => {
      mutations.forEach(mut => {
        if (mutation.secuence == mut.secuence) isCreated = true;
      });
    });
    if (isCreated == undefined)
      return this.http
        .post<IMutation>(this.path, mutation, httpOptions)
        .pipe(catchError(this.handleError));
    else return null;
  }
  updateMutation(mutation: IMutation): Observable<IMutation> {
    return this.http.put<IMutation>(this.path + mutation.id, mutation, httpOptions);
  }
  private handleError(error: HttpErrorResponse) {
    let ErrorMessage = "";
    if (error.error instanceof ErrorEvent)
      ErrorMessage = "Client Error: " + error.error.message;
    else ErrorMessage = `Server Error: ${error.status} ${error.error} +`;
    return throwError(ErrorMessage);
  }
  getStats(): Observable<any[]>{
    return this.http.get<any[]>("").pipe(retry(1))

  }
  getStatsOfMutation(secuence:any): Observable<any>{
    let requestUrl='https://floating-retreat-07648.herokuapp.com/stats/'+secuence
    return this.http.get<any[]>(requestUrl).pipe(retry(1))
  }
  hasMutation(secuence:string): Observable<any>{
    let requestUrl='https://floating-retreat-07648.herokuapp.com/hasMutation/'+secuence
    return this.http.get<any>(requestUrl).pipe(retry(1));
  }

}
  