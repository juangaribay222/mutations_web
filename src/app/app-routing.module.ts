import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MutationComponent } from './Component/mutation/mutation.component';

const routes: Routes = [
  { path: 'mutations', component: MutationComponent},
  { path: '', redirectTo: "/mutations", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
